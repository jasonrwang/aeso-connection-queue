# -*- coding: utf-8 -*-
# %% [markdown]
# # AESO Connection Queue Analysis
#
# This project's purpose is to create an automated way to download data from the Alberta Electric System Operator's (AESO) Project Connection List, which is essentially a database of the electricity projects have been proposed to be connected at some point.
#
# The AESO provides information on all these projects in a monthly Excel sheet that needs cleaning.

# %% [markdown]
# ## Python Packages
#
# Python is a flexible coding language. Here, we use four add-on "packages" to enable the data analysis we want to do:
#
# - Pandas for data cleaning, shaping, and analysis
# - Altair for data visualization
# - Requests for accessing the AESO webpage
# - BeautifulSoup for looking through the AESO webpage for the files we want

# %%
import pandas as pd
import altair as alt
import requests
from bs4 import BeautifulSoup

# %% [markdown]
# ## Data Collection

# %% [markdown]
# Find the latest connection project list from the AESO's website.

# %%
page_name = 'https://www.aeso.ca/grid/transmission-projects/connection-project-reporting/'
page = requests.get(page_name)

# %%
soup = BeautifulSoup(page.text, 'html.parser')

# Find the latest project list - it's a link with the label 'Connection Project List'
for i in soup.find_all("a", rel="noopener"):
    if i.string == 'Connection Project List':
        cxnProjectList = 'https://aeso.ca/' + i.get('href')
        break

# %% [markdown]
# Load the data into a Pandas DataFrame.

# %%
df = pd.read_excel(cxnProjectList,skiprows=1)

# %% [markdown]
# Filter for only the Active rows.
#
# * This section assumes the AESO list will maintain its "Processed for Cluster," "Active," "ISD Under Review," ... format
#
# Find where Active and ISD Under Review begin – these are blank in the "Planning Area" columns
#
# *Note – we actually want the other ones as well, but I have not updated this to include that yet*

# %%
ActiveRows = df[df['Planning Area'].isna()].index[1:3].to_list()
dfActive = df.iloc[ActiveRows[0]+1:ActiveRows[1]-1]
dfActive.head()

# %% [markdown]
# ## Data Cleaning

# %%
# ## Clean and transform data
today = pd.to_datetime('today').normalize()
dfActive['Wait Time'] = (today - dfActive['Applied On'])
dfActive['Wait Time'] = dfActive['Wait Time'].apply(lambda x: x.days) # Just keep number of days

# %%
# Fix data issues
dfActive['ISD'] = pd.to_datetime(dfActive['ISD']) # ISD column isn't read as datetime


# %% [markdown]
# ## Data Analysis

# %%
# Create a string to note when charts were last updated
todayStr = str(today.date())

# %%

# %% [markdown]
# ## Data Visualization

# %% [markdown]
# There are many things to explore!

# %%
ChartConnectionQueue = alt.Chart(dfActive).mark_bar().encode(
    x='MW Type:O',
    y='STS MW:Q',
    # color='Industry Group:N'
).properties(
    title={
        'text':'Alberta Projects in Development',
        'subtitle':['Alberta Electric System Operator | Visualization: @jasonrwang',f"Last Updated: {todayStr}"]
    }
).transform_filter(
    (alt.datum['Stage'] > 0) & (alt.datum['Stage'] < 5),
#   alt.datum['MW Type'] is in 'Wind', 'Solar', 'Solar + Storage'
)
ChartConnectionQueue

# %%
ChartISD = alt.Chart(dfActive).mark_bar().encode(
    x='ISD:T',
    y='STS MW:Q',
    color='MW Type:N'
).properties(
    title={
        'text':'Alberta Projects in Development by Proposed In-Service Date',
        'subtitle':['Alberta Electric System Operator | Visualization: @jasonrwang',f"Last Updated: {todayStr}"]
    }
).transform_filter(
    (alt.datum['Stage'] > 0) & (alt.datum['Stage'] < 5),
#   alt.datum['MW Type'] is in 'Wind', 'Solar', 'Solar + Storage'
)
ChartISD

# %%
ChartProposed = alt.Chart(dfActive).mark_bar().encode(
    x='Applied On:T',
    y='STS MW:Q',
    color='MW Type:N'
).properties(
    title={
        'text':'Alberta Projects in Development by Application Date',
        'subtitle':['Alberta Electric System Operator | Visualization: @jasonrwang',f"Last Updated: {todayStr}"]
    }
).transform_filter(
    (alt.datum['Stage'] > 0) & (alt.datum['Stage'] < 5),
#   alt.datum['MW Type'] is in 'Wind', 'Solar', 'Solar + Storage'
)
ChartProposed

# %%
ChartWaitTime = alt.Chart(dfActive).mark_rect().encode(
    x=alt.X('Wait Time:Q', title='Days').bin(maxbins=60),
    y=alt.Y('MW Type:O'),
    color=alt.Color('count():O', title='Number of Projects')
).properties(
    title={
        'text':'Time in Queue for Alberta Projects in Development',
        'subtitle':['Alberta Electric System Operator | Visualization: @jasonrwang',f"Last Updated: {todayStr}"]
    }
).transform_filter(
    (alt.datum['Stage'] > 0) & (alt.datum['Stage'] < 5),
)
ChartWaitTime

# %%
# Save visualization - if needed
# chart.save('AESO_Queue.png',scale_factor=2.0)

# %%
