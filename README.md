# AESO Queue Calculations

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jasonrwang%2Faeso-connection-queue/HEAD)

Tremendous growth in the electricity sector means gigawatts of projects are waiting to be connected. This project processes the list of 

## If Running in VS Code

1. `conda env create --name AESOQueue --file requirements.txt`
2. `conda activate AESOQueue`
3. `python -m ipykernel install --user --name=AESOQueue`


## To Do

- [ ] Plot latest month
  - [x] Number of stages 1-4
  - [ ] Jobs
  - [ ] Investment $
- [ ] Track project status changes over several months
  - [x] Determine how long projects are in queue on avg
  - [ ] How many projects in % make it to stage 5